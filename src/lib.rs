//===============================================

pub fn radix_sort( data: &mut [ i32 ] )
{
  let mut placement = 1;
  let mut passes    = 0;
  let mut bucket    = Vec::with_capacity( data.len() );

  // Calculate the highest number of bits.
  for n in data.iter()
  {
    use std::mem::size_of_val;

    let leading_zeros = n.leading_zeros() as usize;
    let bits_in_n     = size_of_val( n ) * 8;

    if leading_zeros == 0
    {
      passes = bits_in_n;
      break;
    }

    let msb = bits_in_n - leading_zeros;

    passes =
      if msb > passes { msb    }
      else            { passes };
  }
  let passes = passes;


  for pass in 0..passes
  {
    // NOTE(rope_): Sort the numbers into buckets based on the bit we are currently
    //              considering.
    for i in 0..data.len()
    {
      let value = data[ i ];
      let bit   = ( ( value & placement ) >> pass ) as usize;

      // If the bit is zero then move the number to the next free spot at
      // the beginning of the array.
      if bit == 0
      {
        data[ i - bucket.len() ] = value;
      }

      // If the bit is one then push it onto the end of the bucket array.
      else
      {
        bucket.push( value );
      }
    }

    // NOTE(rope_): Copy the bucket to the end of the data and clear
    //              the bucket for next time.
    let index = data.len() - bucket.len();
    data[ index.. ].copy_from_slice( &bucket );
    bucket.clear();

    // NOTE(rope_): Move to the next most significant bit.
    placement *= 2;
  }
}

//===============================================

#[cfg(test)]
mod tests
{
  #[test]
  fn test()
  {
    use radix_sort;

    let mut set1 = [ 12  , 121 , 55  , 1910, 1121 ];
    let mut set2 = [ 6   , 42  , 308 , 5243, 202  ];
    let mut set3 = [ 113 , 1423, 46  , 2   , 8221 ];
    let mut set4 = [ 7   , 9015, 50  , 4958, 3159 ];

    radix_sort( &mut set1 );
    radix_sort( &mut set2 );
    radix_sort( &mut set3 );
    radix_sort( &mut set4 );

    assert_eq!( set1, [ 12, 55, 121 , 1121, 1910 ] );
    assert_eq!( set2, [ 6 , 42, 202 , 308 , 5243 ] );
    assert_eq!( set3, [ 2 , 46, 113 , 1423, 8221 ] );
    assert_eq!( set4, [ 7 , 50, 3159, 4958, 9015 ] );
  }
}
